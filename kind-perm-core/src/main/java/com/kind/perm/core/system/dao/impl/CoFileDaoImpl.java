package com.kind.perm.core.system.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.CoFileDao;
import com.kind.perm.core.system.domain.CoFileDO;

/**
 * 
 * 小区数据访问实现类. <br/>
 * 
 * @date:2016年12月11日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class CoFileDaoImpl extends BaseDaoMyBatisImpl<CoFileDO, Serializable> implements CoFileDao {



	@Override
	public int saveOrUpdate(CoFileDO entity) {
//		KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
			 return super.insert(NAMESPACE + "insert", entity);
	}
	
	@Override
	public List<CoFileDO> getCoFileDOListByTypeBAndTypeBId(String objectA,String objectB,Long objectBId)
	{
		CoFileDO entity = new CoFileDO();
		entity.setObjectA(objectA);
		entity.setObjectB(objectB);
		entity.setObjectBId(objectBId);
		return super.query(NAMESPACE + "getCoFileDOListByTypeBAndTypeBId", entity);
	}

	@Override
	public void deleteByMap(Map map) {
		deleteByMap(NAMESPACE + "deleteByObjectAId",map);
		
	}

}
