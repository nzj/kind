package com.kind.perm.core.system.dao.impl;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.DataSourceKey;
import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.system.dao.FileDao;
import com.kind.perm.core.system.domain.FileDO;

/**
 * 
 * 小区数据访问实现类. <br/>
 * 
 * @date:2016年12月11日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class FileDaoImpl extends BaseDaoMyBatisImpl<FileDO, Serializable> implements FileDao {



	@Override
	public int saveOrUpdate(FileDO entity) {
//		KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			 return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}
	
	
	@Override
	public FileDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}


	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
		
	}

}
