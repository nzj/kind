package com.kind.perm.netty.codec;

import com.kind.perm.netty.proto.Request;
import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Unpack;

/**
 * Request请求解码类
 * User: 李明
 * Date: 2015/12/10
 * Time: 17:34
 * To change this template use File | Settings | File Templates.
 */
public class RequestDecoder extends AbstractDecoder {

    @Override
    public Marshallable decode(Unpack unpack) {
        Request proto = new Request();
        proto.unmarshal(unpack);
        return proto;
    }
}
