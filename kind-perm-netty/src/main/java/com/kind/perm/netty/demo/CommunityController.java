package com.kind.perm.netty.demo;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.demo.service.CommunityService;
import com.kind.perm.netty.Param;
import com.kind.perm.netty.Path;

/**
 * 
 * Function:小区信息管理控制器. <br/>
 * 
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller("community")
public class CommunityController {
	
	@Resource
	private CommunityService communityService;

	 @Path
	 public JSONObject hello(@Param String name,@Param int age) {
        System.out.println(name);
	    System.out.println(age); 
	    CommunityDO communityDO  = communityService.getById((long)1);
	    JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", communityDO.getAreaName());
        jsonObject.put("name", communityDO.getCityName());
	    
	   return  jsonObject;  
	    }
}
