<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/plugins/code/prettify.css" />
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainform" action="${ctx}/system/area/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">区域信息基本信息</span>
								</div>
								<table class="kv-table" cellspacing="10">
								<input type="hidden" name="id"
											value="${entity.id }" /> 
																		<tr>
										<td class="kv-label">区域编码：</td>
										<td class="kv-content">
											<input type="text" id="code" name="code" value="${entity.code}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">区域名称：</td>
										<td class="kv-content">
											<input type="text" id="name" name="name" value="${entity.name}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">上级编码：</td>
										<td class="kv-content">
											<input type="text" id="pcode" name="pcode" value="${entity.pcode}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">缩写名称：</td>
										<td class="kv-content">
											<input type="text" id="shortName" name="shortName" value="${entity.shortName}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">级别：</td>
										<td class="kv-content">
											<input type="text" id="levelType" name="levelType" value="${entity.levelType}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">区域电话编号：</td>
										<td class="kv-content">
											<input type="text" id="cityCode" name="cityCode" value="${entity.cityCode}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">邮政编码：</td>
										<td class="kv-content">
											<input type="text" id="zipCode" name="zipCode" value="${entity.zipCode}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">全部地区信息：</td>
										<td class="kv-content">
											<input type="text" id="mergerName" name="mergerName" value="${entity.mergerName}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">经度：</td>
										<td class="kv-content">
											<input type="text" id="lng" name="lng" value="${entity.lng}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">纬度：</td>
										<td class="kv-content">
											<input type="text" id="lat" name="lat" value="${entity.lat}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">拼音编码：</td>
										<td class="kv-content">
											<input type="text" id="pinyin" name="pinyin" value="${entity.pinyin}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script  src="${ctx}/static/plugins/kindeditor/kindeditor.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/plugins/code/prettify.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {

			$('#mainform').form({
				onSubmit : function() {
					var isValid = $(this).form('validate');
					// 返回false终止表单提交
					return isValid;
				},
				success : function(data) {
					var dataObj = eval("(" + data + ")");//转换为json对象
					if (submitSuccess(dataObj, dg, d)) {
						dg.treegrid('reload');
					}
				}
			});
		});
	</script>
	
	<!-- 富文本 end -->
</body>
</html>