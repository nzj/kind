<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/plugins/code/prettify.css" />
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainform" action="${ctx}/system/dict/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">字典信息基本信息</span>
								</div>
								<table class="kv-table" cellspacing="10">
								<input type="hidden" name="id"
											value="${entity.id }" /> 
																		<tr>
										<td class="kv-label">名称：</td>
										<td class="kv-content">
											<input type="text" id="label" name="label" value="${entity.label}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">参数值：</td>
										<td class="kv-content">
											<input type="text" id="value" name="value" value="${entity.value}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">类型：</td>
										<td class="kv-content">
											<input type="text" id="type" name="type" value="${entity.type}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">类型描述：</td>
										<td class="kv-content">
											<input type="text" id="description" name="description" value="${entity.description}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">排序：</td>
										<td class="kv-content">
											<input type="text" id="sortInfo" name="sortInfo" value="${entity.sortInfo}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">备注：</td>
										<td class="kv-content">
											<input type="text" id="remark" name="remark" value="${entity.remark}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">是否删除：</td>
										<td class="kv-content">
											<input type="text" id="delFlag" name="delFlag" value="${entity.delFlag}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>

								<!-- 上传控件 star -->
									
								<!-- 上传控件 end -->
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script  src="${ctx}/static/plugins/kindeditor/kindeditor.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/plugins/code/prettify.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {

			$('#mainform').form({
				onSubmit : function() {
					var isValid = $(this).form('validate');
					// 返回false终止表单提交
					return isValid;
				},
				success : function(data) {
					var dataObj = eval("(" + data + ")");//转换为json对象
					if (submitSuccess(dataObj, dg, d)) {
						dg.treegrid('reload');
					}
				}
			});
		});
	</script>

	<!-- 文件上传 star -->
	
	<!-- 文件上传 end -->
	<!-- 富文本 star -->
	<script type="text/javascript" charset="UTF-8">
		$(function() {
			







		}); 
	</script>
	
	<!-- 富文本 end -->
</body>
</html>