<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>
<body class="easyui-layout" style="font-family: '微软雅黑'">
	<div data-options="region:'west',split:true,border:false,title:'导出模板列表'"  style="width: 300px">
	
	
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'north',split:true,border:false" style="height:60px">
		    	<form id="searchTempletFrom" action="">
	                <input type="text" name="name" class="easyui-validatebox" data-options="width:150,prompt: '导出模板名称'"/>
	                <span class="toolbar-item dialog-tool-separator"></span>
	                <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="searchTemplet();">查询</a>
				</form>
				<shiro:hasPermission name="tablecustomtemplet:tablecustomtemplet:save">
					<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addTemplet();">添加</a>
		            <span class="toolbar-item dialog-tool-separator"></span>
	            </shiro:hasPermission>
	            <shiro:hasPermission name="tablecustomtemplet:tablecustomtemplet:change">
		            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateTemplet();">修改</a>
		            <span class="toolbar-item dialog-tool-separator"></span>
	            </shiro:hasPermission>
	            <shiro:hasPermission name="tablecustomtemplet:tablecustomtemplet:view">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="viewTemplet();">查看</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
	            </shiro:hasPermission>
	            <shiro:hasPermission name="tablecustomtemplet:tablecustomtemplet:remove">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeTemplet();">删除</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
	            </shiro:hasPermission>
			</div>
			
			<div data-options="region:'center',border:false">
				<table id="templetDg"></table>
			</div>
		</div>
    </div>   
    
    
    
    <div data-options="region:'center',split:true,border:false,title:'字段设置'">
    	<div id="tg_tb" style="padding:5px;height:auto">
		    <div>
		    	<shiro:hasPermission name="tablecustom:tablecustom:save">
			    	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="addCustom()">添加</a>
			    	<span class="toolbar-item dialog-tool-separator"></span>
		    	</shiro:hasPermission>
		    	<shiro:hasPermission name="tablecustom:tablecustom:change">
		            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateCustom();">修改</a>
		            <span class="toolbar-item dialog-tool-separator"></span>
				</shiro:hasPermission>
				<shiro:hasPermission name="tablecustom:tablecustom:view">
		            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="viewCustom();">查看</a>
		            <span class="toolbar-item dialog-tool-separator"></span>
	            </shiro:hasPermission>
	            <shiro:hasPermission name="tablecustom:tablecustom:remove">
			        <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-remove" onclick="removeCustom()">删除</a>
			        <span class="toolbar-item dialog-tool-separator"></span>
		        </shiro:hasPermission>
		    </div>
		</div>
    	<table id="dgCourse"></table>
    </div>   
<div id="dlg"></div> 
<div id="icon_dlg"></div>

<!--添加、修改模板窗体-->
<div id="dlgTemplet"></div>

<script type="text/javascript">
var dgTemplet;
var dTemplet;
var dgCourse;
var dCourse;

var tbType=0;
$(function(){   
	dgTemplet=$('#templetDg').treegrid({  
	method: "get",
    url:'${ctx}/tableCustom/tableCustom/selectAllTemplet', 
    fit : true,
	fitColumns : true,
	border : false,
	idField : 'id',
	treeField:'name',
	parentField : 'parentId',
	iconCls: 'icon',
	animate:true, 
	rownumbers:true,
	singleSelect:true,
	striped:true,
    columns:[[
		{field:'ck',checkbox:true },
        {field:'id',title:'id',hidden:true},
        {field:'type',title:'模板类型',},
        {field:'name',title:'模板名称',width:100}
    ]],
    enableHeaderClickMenu: false,
    enableHeaderContextMenu: false,
    enableRowContextMenu: false,
    dataPlain: true,
    onLoadSuccess: function(data){
    	if(data){
    		$.each(data.rows,
    		function(index,item){
    		  if(item.checked){
    			$('#dgCourse').datagrid('checkRow',index);
    		  }
    		});
    	}
    },
    onClickRow:function(rowData){
    	tbType=rowData.type;
    	dgCourse.datagrid('reload',{tbType:tbType});
    }
	});
	
	dgCourse=$('#dgCourse').datagrid({   
	method: "get",
	url:'${ctx}/tableCustom/tableCustom/getCustomByTempletId',
    fit : true,
	fitColumns : true,
	border : false,
	idField : 'id',
	treeField:'name',
	parentField : 'parentId',
	iconCls: 'icon',
	animate:true, 
	rownumbers:true,
	singleSelect:true,
	striped:true,
    columns:[[
		{field:'ck',checkbox:true },
        {field:'id',title:'id',hidden:true,width:100},    
        {field:'fieldSort',title:'顺序号',width:100},
        {field:'fieldName',title:'表字段对应的javaBean属性名称',width:100},
        {field:'fieldType',title:'属性类型',width:100,formatter: function (val, row, index) {
        	if(val == "1"){
        		return "文本";
        	}else if(val == "2"){
        		return "数字";
        	}else if(val == "3"){
        		return "日期 YYYY-MM-dd";
        	}else if(val == "4"){
        		return "日期YYYY-MM-dd HH:mm:ss";
        	}
        }},
        {field:'fieldTitle',title:'默认的显示名',width:100},
        {field:'fieldAnotherTitle',title:'显示的别名，如果没有则显示默认名称',width:100},
        {field:'isExport',title:'是否导出',width:100, formatter: function (val, row, index) {
        	if(val == "1"){
        		return "<span class='icon-ok'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}else{
        		return "<span class='icon-standard-cross'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}
        }},
        {field:'isPrint',title:'是否打印',width:100, formatter: function (val, row, index) {
        	if(val == "1"){
        		return "<span class='icon-ok'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}else{
        		return "<span class='icon-standard-cross'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}
        }},
        {field:'isShow',title:'是否显示',width:100, formatter: function (val, row, index) {
        	if(val == "1"){
        		return "<span class='icon-ok'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}else{
        		return "<span class='icon-standard-cross'>&nbsp;&nbsp;&nbsp;&nbsp;</span>";
        	}
        }},
        
    ]],
    onLoadSuccess: function(data){
    	if(data){
    		$.each(data.rows,
    		function(index,item){
    		  if(item.checked){
    			$('#dgCourse').datagrid('checkRow',index);
    		  }
    		});
    	}
    },
    toolbar:'#tg_tb',
    dataPlain: true
	});
	
});




//////////       模板 开始                                   ////////////////
/**
 * 查询模板
 */
function searchTemplet(){
	var formObj=$("#searchTempletFrom").serializeObject();
	dgTemplet.treegrid('load',formObj);
	
	//查询模板时 把已经显示的模板字段列表清空
	dgCourse.datagrid('loadData',{total:0,rows:[]})
	
}


//弹窗增加模板
function addTemplet() {
    dTemplet=$("#dlgTemplet").dialog({
        title: '添加特权信息',
        width: 500,
        height: 500,
        href:'${ctx}/tableCustom/tableCustom/toAddTemplet',
        maximizable:true,
        modal:true,
        buttons:[{
            text:'确认',
            class: 'easyui-linkbutton c6',
            iconCls: 'icon-ok',
            handler:function(){
                $("#mainformTemplet").submit();
            }
        },{
            text:'取消',
            iconCls: 'icon-cancel',
            handler:function(){
            	dTemplet.panel('close');
            }
        }]
    });
}

//弹窗修改模板
function updateTemplet(){
    var row = dgTemplet.datagrid('getSelected');
    if(rowIsNull(row)) return;
    dTemplet=$("#dlgTemplet").dialog({
        title: '修改特权信息',
        width: 500,
        height: 500,
        href:'${ctx}/tableCustom/tableCustom/updateTemplet/'+row.id,
        maximizable:true,
        modal:true,
        buttons:[{
            text:'修改',
            iconCls: 'icon-ok',
            handler:function(){
                $('#mainformTemplet').submit();
            }
        },{
            text:'取消',
            iconCls: 'icon-cancel',
            handler:function(){
            	dTemplet.panel('close');
            }
        }]
    });
}

//弹窗查看模板
function viewTemplet(){
    var row = dgTemplet.datagrid('getSelected');
    if(rowIsNull(row)) return;
    dTemplet=$("#dlgTemplet").dialog({
        title: '查看特权信息',
        width: 800,
        height: 800,
        href:'${ctx}/tableCustom/tableCustom/viewTemplet/'+row.id,
        maximizable:true,
        modal:true,
        buttons:[]
    });
}
//删除模板
function removeTemplet(){
    var row = dgTemplet.datagrid('getSelected');
    if(rowIsNull(row)) return;
    parent.$.messager.confirm('提示', '删除后无法恢复，您确定要删除吗？', function(data){
        if (data){
            $.ajax({
                type:'get',
                url:"${ctx}/tableCustom/tableCustom/removeTemplet/"+row.id,
                success: function(data){
                	//如果ret为0，则表示成功，非0的都表示不成功
                    if(data && data.ret == 0){
                        dgTemplet.treegrid('reload');
                        parent.$.messager.show({ title : "提示",msg: "操作成功！", position: "bottomRight" });
                    }else {
                        parent.$.messager.alert(data.msg);
                    }
                }
            });
        }
    });
}

//////////       模板 结束                                   ////////////////



//////////       模板字段信息开始                                   ////////////////


//弹窗增加模板字段
function addCustom() {
	if(rowIsNull(tbType)) return;
	dCourse=$('#dlg').dialog({
	    title: '添加字段',    
	    width: 800,    
	    height: 700,    
	    maximizable:true,
	    resizable:true,
	    href:'${ctx}/tableCustom/tableCustom/addCustom/' + tbType,
	    modal: true,
	    buttons:[{
            text:'添加',
            iconCls: 'icon-ok',
            handler:function(){
                $('#mainformCustom').submit();
            }
        },{
            text:'取消',
            iconCls: 'icon-cancel',
            handler:function(){
            	dCourse.panel('close');
            }
        }]
	});
}

//弹窗修改模板字段信息
function updateCustom(){
	var row = dgCourse.datagrid('getSelected');
    if(rowIsNull(row)) return;
    dCourse=$('#dlg').dialog({
        title: '修改字段信息',
        width: 800,
        height: 800,
        href:'${ctx}/tableCustom/tableCustom/updateCustom/'+row.id,
        maximizable:true,
        modal:true,
        buttons:[{
            text:'修改',
            iconCls: 'icon-ok',
            handler:function(){
                $('#mainformCustom').submit();
            }
        },{
            text:'取消',
            iconCls: 'icon-cancel',
            handler:function(){
            	dCourse.panel('close');
            }
        }]
    });
}

//弹窗查看模板字段信息
function viewCustom(){
	var row = dgCourse.datagrid('getSelected');
    if(rowIsNull(row)) return;
    dCourse=$('#dlg').dialog({
        title: '查看字段信息',
        width: 800,
        height: 800,
        href:'${ctx}/tableCustom/tableCustom/viewCustom/'+row.id,
        maximizable:true,
        modal:true,
        buttons:[]
    });
}

/**
 *删除模板字段信息
 */
function removeCustom(){
	var row = dgCourse.datagrid('getSelected');
	if(rowIsNull(row)) return;
	parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function(data){
		if (data){
			$.ajax({
				type:'get',
				url:"${ctx}/tableCustom/tableCustom/removeCustom/"+row.id,
				success: function(data){
					//如果ret为0，则表示成功，非0的都表示不成功
                    if(data && data.ret == 0){
                        dgCourse.datagrid('reload');
                        parent.$.messager.show({ title : "提示",msg: "操作成功！", position: "bottomRight" });
                    }else {
                        parent.$.messager.alert(data.msg);
                    }
				}
			});
			//dgCourse.datagrid('reload'); //grid移除一行,不需要再刷新
		} 
	});

}


//////////       模板字段信息结束                                  ////////////////
</script>
</body>
</html>