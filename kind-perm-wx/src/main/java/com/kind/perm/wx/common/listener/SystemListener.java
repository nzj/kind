package com.kind.perm.wx.common.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kind.common.uitls.WxUtils;

import weixin.popular.support.TicketManager;
import weixin.popular.support.TokenManager;




/**
 * 系统配置加载监听器
 * 
 * @author 
 * 
 */
public class SystemListener implements ServletContextListener {
	
	private Logger logger = LoggerFactory.getLogger(SystemListener.class);
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	public void contextInitialized(ServletContextEvent arg0) {
		//微信服务器权限 调试关闭
//		loadTokenManager();
//		loadTicketManager();
		
	}
	
	/**
	 * 加载微信相关
	 */
	public void loadTokenManager(){
		//token init
		TokenManager.init(WxUtils.WX_appid, WxUtils.WX_appsecret); //appid appsecret

	}
	public void loadTicketManager(){
		String token = "";
		if(token==null || "".equals(token)){
			do{
				logger.error("loadTicketManager:token is null");
				try {
					Thread.sleep(1000);
					token = TokenManager.getDefaultToken();
				} catch (Exception e) {
				}
			}while(token==null || "".equals(token));
		}
		
		logger.error("loadTicketManager...token:["+TokenManager.getDefaultToken()+"]");;

		TicketManager.init(WxUtils.WX_appid);//appid
	}

}
